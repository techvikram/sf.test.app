Overview of this application
- Uses React Hooks to maintain State where needed
- fetch API is used to GET the data from end point.
- User Container/Presentation components models

## Containers (~/containers/)
| Container Component | Comments |
| ------ | ------ |
| Debts.js | A container component which handles module state, data fetch, check box click events, add new record event, delete record event. It passes required data and callback methods to underlying presentation/composite components |

## Components (~/components/)

| Component | Comments |
| ------ | ------ |
| App.js | Root component for the App. Consumes ~/containers/Debts.js container component |
| Debts.js | Composite component which accepts the data and event handling functions from ~/containers/Debts.js container component and renders Debts Table, Add button, Remove button, Balance Summary by passing in appropriate data as props to those presentation components  |
|DebtsTable.js| Presentation component which renders the table and check box cloumns in it|
|BalanceSummary.js| Presentation component which calculates and displays Total Balance, Total rows count, Checked rows count based on the data provide in input props|
|DebtAdd.js| Presentation component which renders TextInput fields required to create new debt record. It also accepts event handler functions to handle click on Add and Cancel buttons|
|DebtDelete.js| Presentation component which renders readonly DebtsTable and prompts user for confirmation. It also accepts event handler functions to handle click on Delete and Cancel buttons|


