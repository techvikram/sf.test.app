import React from "react";
import { Alert, Col } from "react-bootstrap";

export default ({ debts, checkedDebts, formatter }) => {
  const total = debts.reduce((sum, debt) => {
    if (checkedDebts.includes(debt.id)) return sum + debt.balance;
    else return sum;
  }, 0);
  return (
    <>
      <Alert variant="primary">
        <div className="d-flex justify-content-between">
          <div className="p-2 col-example text-left">Total Balance:</div>
          <div className="p-2 col-example text-left">
            {formatter.format(total)}
          </div>
        </div>
      </Alert>
      <div className="d-flex justify-content-between">
        <div className="p-2 col-example text-left">
          Total Row Count: {debts.length}
        </div>
        <div className="p-2 col-example text-left">
          Check Row Count: {checkedDebts.length > debts.length ? debts.length : checkedDebts.length}
        </div>
      </div>
    </>
  );
};
