import React, { useState } from "react";
import { Row, Col, Button } from "react-bootstrap";
import TextInput from "./TextInput";

export default ({ handleCancel, handleAdd }) => {
  const [creditorName, setCreditorName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [minPaymentPercentage, setMinPaymentPercentage] = useState(null);
  const [balance, setBalance] = useState(null);
  
  const handleSave = () => {
      handleAdd(creditorName,firstName,lastName,Number(minPaymentPercentage),Number(balance));
  }

  return (
    <>
      <Row>
        <Col>
          <div class="form-group">
            <label for="creditorName">Creditor Name:</label>
            <TextInput
              name="creditorName"
              value={creditorName}
              handleTextChange={(name, value) => setCreditorName(value)}
            />
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <label for="firstName">Firstame:</label>
          <TextInput
            name="firstName"
            value={firstName}
            handleTextChange={(name, value) => setFirstName(value)}
          />
        </Col>
        <Col>
          <div class="form-group">
            <label for="lastName">Lastname:</label>
            <TextInput
              name="lastName"
              value={lastName}
              handleTextChange={(name, value) => setLastName(value)}
            />
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <label for="minPaymentPercentage">Payment %:</label>
          <TextInput
            type="number"
            name="minPaymentPercentage"
            value={minPaymentPercentage}
            handleTextChange={(name, value) => setMinPaymentPercentage(value)}
          />
        </Col>
        <Col>
          <div class="form-group">
            <label for="balance">Balance:</label>
            <TextInput
              type="number"
              name="balance"
              value={balance}
              handleTextChange={(name, value) => setBalance(value)}
            />
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="pt-3">
            <Button variant="primary" size="sm" onClick={handleSave}>
              Add
            </Button>{" "}
            <Button variant="danger" size="sm" onClick={handleCancel}>
              Cancel
            </Button>
          </div>
        </Col>
      </Row>
    </>
  );
};
