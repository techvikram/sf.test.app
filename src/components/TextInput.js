import React from "react";

export default ({ name, value, handleTextChange,  type="text" }) => {
  const myChangeHandler = (event) => {
      console.log('event.target->', event.target);
    handleTextChange(event.target.name, event.target.value);
  };
  return (
    <input
      id={name}
      name={name}
      type={type}
      value={value}
      onChange={myChangeHandler}
      className="form-control"
    />
  );
};
