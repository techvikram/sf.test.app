import React from "react";
import DebtsTable from "../components/DebtsTable";
import { Row, Col, Button } from "react-bootstrap";

export default ({
  debts,
  checkedDebts,
  currencyFormatter,
  percentageFormatter,
  handleCancel,
  handleDeleteConfirm
}) => {
  if (debts) {
      const selectedDebts = debts.filter(debt => checkedDebts.includes(debt.id));
    return (
      <>
        <Row>
          <Col>
            <DebtsTable
              debts={selectedDebts}
              formatter={currencyFormatter}
              percentageFormatter={percentageFormatter}
              title="Slected Debts"
              showCheckboxes={false}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="pb-2">
              <p>Are you sure you want to delete these debts?</p>
              <Button variant="primary" size="sm" onClick={handleDeleteConfirm}>
                Delete
              </Button>{" "}
              <Button variant="danger" size="sm" onClick={handleCancel}>
                Cancel
              </Button>
            </div>
          </Col>
        </Row>
      </>
    );
  } else {
    return <div>Empty content</div>;
  }
};
