import React from "react";
import { Table } from "react-bootstrap";
import CheckboxInput from "./CheckBoxInput";
import TextInput from "./TextInput";

const DebtDataRowEmpty = () => {
  return (
    <tr className="dataRow">
      <td>checkbox</td>
      <td className="dataCol">
        <TextInput name="creditor" />
      </td>
      <td className="dataCol">
        <TextInput name="firstname" />
      </td>
      <td className="dataCol">
        <TextInput name="lastname" />
      </td>
      <td className="dataCol">
        <TextInput name="paymentpercent" />
      </td>
      <td className="dataCol">
        <TextInput name="balance" />
      </td>
    </tr>
  );
};

const DebtDataRow = ({
  id,
  creditorName,
  firstName,
  lastName,
  minPaymentPercentage,
  balance,
  checked,
  checkedDebts,
  handleCheckboxChange,
  formatter,
  percentageFormatter,
  showCheckboxes,
}) => {
  const isChecked =
    showCheckboxes && (checkedDebts.includes(id) || checkedDebts.includes(-1));
  return (
    <tr className="dataRow" key={id}>
      {showCheckboxes && (
        <td>
          <CheckboxInput
            id={id}
            name="debtRowSlector"
            checked={isChecked}
            handleChange={handleCheckboxChange}
          />
        </td>
      )}
      <td className="dataCol">{creditorName}</td>
      <td className="dataCol">{firstName}</td>
      <td className="dataCol">{lastName}</td>
      <td className="dataCol">
        {percentageFormatter.format(minPaymentPercentage / 100)}
      </td>
      <td className="dataCol">{formatter.format(balance)}</td>
    </tr>
  );
};
export default ({
  debts,
  checkedDebts,
  title,
  handleCheckedDebts,
  formatter,
  percentageFormatter,
  showCheckboxes = true,
}) => {
  const debtRows = debts.map((debt) => (
    <DebtDataRow
      {...debt}
      handleCheckboxChange={handleCheckedDebts}
      checkedDebts={checkedDebts}
      formatter={formatter}
      percentageFormatter={percentageFormatter}
      showCheckboxes={showCheckboxes}
    />
  ));
  return (
    <>
      <h3>{title}</h3>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            {showCheckboxes && (
              <th>
                <CheckboxInput
                  id={-1}
                  name="debtHeaderSlector"
                  checked={checkedDebts.includes(-1)}
                  handleChange={handleCheckedDebts}
                />
              </th>
            )}
            <th>Creditor</th>
            <th>Firstname</th>
            <th>Lastame</th>
            <th>Min Pay%</th>
            <th>Balance</th>
          </tr>
        </thead>
        <tbody>{debtRows}</tbody>
      </Table>
    </>
  );
};
