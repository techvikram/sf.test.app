import React from "react";
import logo from "../logo.svg";
import { Link, animateScroll } from "react-scroll";
import { Navbar, Nav } from "react-bootstrap";

const scrollToTop = () => {
  animateScroll.scrollToTop();
};

export default () => (
  <Navbar collapseOnSelect expand="lg" className="nav">
    <Navbar.Brand onClick={scrollToTop}>
      <img src={logo} className="nav-logo" alt="Logo" />
      Vikram's Test App
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    <Navbar.Collapse id="responsive-navbar-nav">
      <Nav className="mr-auto">
        {/* <ul className="nav-items">
          <li className="nav-item">
            <Link
              activeClass="active"
              to="section1"
              spy={true}
              smooth={true}
              offset={-70}
              duration={500}
            >
              Demo
            </Link>
          </li>
        </ul> */}
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);
