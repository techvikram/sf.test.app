import React from "react";
import DebtsTable from "../components/DebtsTable";
import BalanceSummary from "../components/BalanceSummary";
import { Row, Col, Button } from "react-bootstrap";


export default ({
    debts,
    checkedDebts,
    handleCheckedDebts,
    handleAddNewClick,
    handleRemoveClick,
    currencyFormatter,
    percentageFormatter,
}) => {
  if (debts) {
    console.log("checkedDebts", checkedDebts);
    console.log("debts->", debts);
    return (
      <>
        <Row>
          <Col>
            <DebtsTable
              debts={debts}
              checkedDebts={checkedDebts}
              title="Outstanding User Debts:"
              handleCheckedDebts={handleCheckedDebts}
              formatter={currencyFormatter}
              percentageFormatter={percentageFormatter}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <div className="pb-2">
              <Button variant="primary" size="sm" onClick={handleAddNewClick}>
                Add New
              </Button>{" "}
              <Button variant="danger" size="sm"  onClick={handleRemoveClick} disabled={checkedDebts.length <= 0}>
                Remove Selected
              </Button>
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <BalanceSummary
              debts={debts}
              checkedDebts={checkedDebts}
              formatter={currencyFormatter}
            />
          </Col>
        </Row>
      </>
    );
  } else {
    return <div>Empty content</div>;
  }
};
