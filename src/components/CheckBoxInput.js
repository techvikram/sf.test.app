import React from "react";

export default ({ name, id, checked = false, handleChange }) => {
    const toggleCheckBox = () => {
        if(handleChange) handleChange(id, !checked);
    };
    return (
      <input
        name = {`${name}-${id}`}
        type="checkbox"
        value={id}
        checked={checked}
        onChange={toggleCheckBox}
      />
    );
  };