import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders site title', () => {
  const { getByText } = render(<App />);
  const titleElement = getByText(/Vikram's Portal/i);
  expect(titleElement).toBeInTheDocument();
});
