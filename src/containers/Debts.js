import React, { useState } from "react";
import Debts from "../components/Debts";
import DebtDelete from "../components/DebtDelete";
import DebtAdd from "../components/DebtAdd";

const fetchDebts = (onSuccess, onError) => {
  // do GET on url using js Fetch
  const url =
    "https://raw.githubusercontent.com/StrategicFS/Recruitment/master/data.json";
  fetch(url)
    .then((response) => {
      // console.log(response);
      return response.json();
    })
    .then((result) => onSuccess(result))
    .catch((err) => onError(err));
};

const currencyFormatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

var percentageFormatter = new Intl.NumberFormat("en-US", {
  style: "percent",
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

export default () => {
  const [debts, setDebts] = useState(null);
  const [checkedDebts, setCheckedDebts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [hasErrored, setHasErrored] = useState(false);
  const [currentView, setCurrentView] = useState("list"); // list || add || delete

  useState(() => {
    setIsLoading(true);
    setHasErrored(false);
    if (!debts) {
      fetchDebts(
        // onSuccess
        (data) => {
          setIsLoading(false);
          setDebts(data);
        },
        // onError
        (err) => {
          setIsLoading(false);
          setHasErrored(true);
          setDebts(null);
        }
      );
    }
  });

  const handleCheckedDebts = (id, isChecked) => {
    if (id === -1) {
      if (isChecked) {
        const newCheckedDebts = debts.map((debt) => debt.id);
        newCheckedDebts.push(-1);
        setCheckedDebts(newCheckedDebts);
      } else setCheckedDebts([]);
    } else {
      if (isChecked) {
        if (!checkedDebts.includes(id)) setCheckedDebts([...checkedDebts, id]);
      } else {
        const newCheckedDebts = checkedDebts.filter(
          (item) => item !== -1 && item !== id
        );
        setCheckedDebts(newCheckedDebts);
      }
    }
  };

  const addDebts = (
    creditorName,
    firstName,
    lastName,
    minPaymentPercentage,
    balance
  ) => {
    const id = debts && debts.length > 0 ? Math.max(...debts.map((debt) => debt.id)) + 1 : 1;
    const newDebts = debts.concat({
      id,
      creditorName,
      firstName,
      lastName,
      minPaymentPercentage,
      balance,
    });
    setDebts(newDebts);
    setCheckedDebts([]);
    setCurrentView("list");
  };

  const deleteDebts = () => {
    const newDebts = debts.filter((debt) => !checkedDebts.includes(debt.id));
    setDebts(newDebts);
    setCheckedDebts([]);
    setCurrentView("list");
  };

  if (hasErrored) {
    return <div>An error occured </div>;
  }

  if (isLoading) {
    return <div>Loading...</div>;
  }

  switch (currentView) {
    case "add":
      return (
        <DebtAdd
          handleAdd={addDebts}
          handleCancel={() => setCurrentView("list")}
        />
      );
    case "delete":
      return (
        <DebtDelete
          debts={debts}
          checkedDebts={checkedDebts}
          currencyFormatter={currencyFormatter}
          percentageFormatter={percentageFormatter}
          handleDeleteConfirm={deleteDebts}
          handleCancel={() => setCurrentView("list")}
        />
      );
    default:
      return (
        <Debts
          debts={debts}
          checkedDebts={checkedDebts}
          handleCheckedDebts={handleCheckedDebts}
          handleAddNewClick={() => setCurrentView("add")}
          handleRemoveClick={() => setCurrentView("delete")}
          currencyFormatter={currencyFormatter}
          percentageFormatter={percentageFormatter}
        />
      );
  }
};
