import React from "react";
import Navbar from "./components/Navbar";
import Debts from "./containers/Debts";
import { Container } from "react-bootstrap";

export default () => (
  <div className="App">
    <Navbar />
    <br />
    <Container>
        <Debts />
      </Container>
  </div>
);
